<h1>Prueba de Dialogo</h1>

<?php
	// 1. preparar los scripts de jQuery:
	$cs = Yii::app()->getClientScript();
	$cs->registerCoreScript('jquery');
	$cs->registerScriptFile($cs->getCoreScriptUrl()."/jui/js/jquery-ui.min.js");
	$cs->registerCssFile($cs->getCoreScriptUrl()."/jui/css/base/jquery-ui.css");
	// validator no lo trae Yii, asi que lo ponemos a mano en folder /JS/
	$cs->registerScriptFile("js/jquery-validate.js");
	$cs->registerScriptFile("js/json2.js");
	$cs->registerScriptFile("js/dialogo1.js");
?>

<?php
	// 2. un simple lanzador del dialogo
?>
<a id='lanzador' style='cursor: pointer;'>Nueva Persona</a>


<?php
	// 3. el codigo del lanzador
?>
<script>
	new Dialogo1(
	{
		idlanzador: "lanzador",
		iddialogo: "dialogo1",
		action: "index.php?r=site/persona",
		logid: "logger"
	}
);</script>


<?php // 4. el layout html del dialogo: ?>
<div id='dialogo1' class='form' style='display: none;'>
	<form id='dialogo1_form'>
		<div class="row">
			<label>Cédula: <span class='required'>*</span></label>
			<input type='text' name='cedula'>
		</div>
		<div class="row">
			<label class='requiered'>Nombre: <span class='required'>*</span></label>
			<input type='text' name='nombre'>
		</div>
		<div class="row">
			<label>Apellido:</label>
			<input type='text' name='apellido'>
		</div>
	</form>
	<div id='logger'>...</div>
</div>
